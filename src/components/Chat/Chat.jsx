import React, {Component} from 'react';
import Header from "../Header/Header";
import MessageList from "../MessageList/MessageList";
import './chat.css';
import logoImg from '../../assets/img/logo.png';
import MessageInput from "../MessageInput/MessageInput";
import Footer from "../Footer/Footer";
import PropTypes from "prop-types";

class Chat extends Component {
	constructor(props) {
		super(props);
		this.state = {
			messages: null,
			text: '',
			isLoading: true,
			ownMessages: [
				{
					id: "80f08600-1b8f-11e8-9629-c7eca82aa987",
					avatar: '',
					text: "I say hello!",
					user: "Bob",
					createdAt: '2020-07-16T19:58:12.936Z'
				},
			],
		};
	}

	componentDidMount() {
		fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
			.then(response => response.json())
			.then(data => {
				if (data) {
					this.setState({
						messages: data,
						isLoading: false,
					});
				}
			})
			.catch(error => console.error(error));
	}

	setOwnMessage(ownMessage) {
		const {ownMessages} = this.state;
		ownMessages.push(ownMessage);
		this.setState(prev => ({
			...prev,
			ownMessages: ownMessages,
		}));
	}

	onChangeOwnMessage(id, text) {
		const ownMessages = this.state.ownMessages;
		const index = ownMessages.findIndex(item => item.id === id);
		ownMessages[index].text = text;
	}

	onDeleteMessage(id) {
		this.setState(previousState => ({
			ownMessages: previousState.ownMessages.filter(message => message.id !== id)
		}))
	}

	render() {
		return (
			<div className='chat-container'>

				<div className='logo-container'>
					<img src={logoImg} alt="chat logo"/>
				</div>

				<div className='header-container'>
					<Header messages={this.state}/>
				</div>

				<div className='content-container'>
					<MessageList
						onDeleteMessage={(id) => this.onDeleteMessage(id)}
						onChangeOwnMessage={(id, text) => this.onChangeOwnMessage(id, text)}

						messages={this.state}
					/>
					<MessageInput
						messages={this.state.messages}
						setOwnMessage={(ownMessage) => this.setOwnMessage(ownMessage)}
						handleSendOwnMessage={() => this.handleSendOwnMessage()}
						handleChangeInput={(event) => this.handleChangeInput(event)}
						inputMessage={this.state.text}
					/>
				</div>

				<div className='footer-container'>
					<Footer/>
				</div>

			</div>
		);
	}
}

Chat.propTypes = {
	messages: PropTypes.arrayOf(
		PropTypes.shape({
			avatar: PropTypes.string,
			createdAt: PropTypes.string,
			editedAt: PropTypes.string,
			id: PropTypes.string,
			text: PropTypes.string,
			user: PropTypes.string,
			userId: PropTypes.string,
		})
	)
}

export default Chat;