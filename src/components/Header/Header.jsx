import React, {Component} from 'react';
import './header.css';
import Preloader from "../Preloader/Preloader";
import Moment from 'react-moment';
import moment from 'moment';

class Header extends Component {
	constructor(props) {
		super(props);
		this.props = props;
	}

	render() {
		const isLoading = this.props.messages.isLoading;
		const messages = this.props.messages.messages;
		const ownMessages = this.props.messages.ownMessages;

		let lastDate = '';
		let lastDateOwnMessages = '';
		let participants = [];
		let isLastDate;

		if (messages !== null && ownMessages !== null) {
			const moments = messages.map(item => moment(item.createdAt));
			lastDate = moment.max(moments);

			const momentsOwnMessages = ownMessages.map(item => moment(item.createdAt));
			lastDateOwnMessages = moment.max(momentsOwnMessages);

			isLastDate = moment(lastDate).isBefore(lastDateOwnMessages, 'second');

			messages.forEach(item => participants.push(item.userId));
			participants = [...new Set(participants)];
		}

		return (
			<div className='header'>
				<div className='header-info-block'>

					<div className='header-title'>
						<span>My chat</span>
					</div>

					<div className='header-users-count'>
						<span>{participants.length} participants</span>
					</div>

					<div className='header-messages-count'>
						<div>
							{isLoading ?
								<Preloader/> :
								this.props.messages.messages.length + this.props.messages.ownMessages.length
							}
						</div>
						<div>
							<span>messages</span>
						</div>
					</div>
				</div>

				<div className='header-last-message-block'>
					<div className='header-last-message-date'>
						<span>Last message at</span>
						<div>
							{isLoading ?
								<Preloader/> :
								<Moment
									format="DD.MM.YYYY HH:mm"
								>
									{isLastDate ?  lastDateOwnMessages : lastDate}
								</Moment>
							}
						</div>
					</div>
				</div>

			</div>
		);
	}
}

export default Header;