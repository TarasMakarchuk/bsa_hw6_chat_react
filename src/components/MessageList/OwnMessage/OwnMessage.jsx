import React, {Component} from 'react';
import './ownMessage.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTrashAlt} from '@fortawesome/free-solid-svg-icons'
import Moment from "react-moment";

class OwnMessage extends Component {
	constructor(props) {
		super(props);
		this.props = props;
	}

	render() {
		const ownMessage = this.props.ownMessages;

		return (
			<div className='own-message'>

				<div className='message-text'> {ownMessage.text}
				</div>

				<div className='own-data-block'>
					<div className='message-time'><Moment
						style={{color: '#775'}}
						format="HH:mm"
					>
						{ownMessage.createdAt}
					</Moment></div>
				</div>

				<div className='message-edit-block'>
					<button className='message-edit'
					        onClick={() => this.props.onChangeOwnMessage(ownMessage.id, ownMessage.text)}>
						<FontAwesomeIcon icon={faEdit} className='edit-icon'/>
					</button>
					<button
						className='message-delete'
						onClick={() => this.props.onDeleteMessage(ownMessage.id)}
					>
						<FontAwesomeIcon icon={faTrashAlt} className='delete-icon'/>
					</button>
				</div>
			</div>
		);
	}
}

export default OwnMessage;