import React from 'react';
import Message from "./Message/Message";
import OwnMessage from "./OwnMessage/OwnMessage";
import Preloader from "../Preloader/Preloader";
import './messageList.css';

class MessageList extends React.Component {
	constructor(props) {
		super(props);
		this.props = props;
	}

	render() {
		const isLoading = this.props.messages.isLoading;
		const messages = this.props.messages.messages;
		const ownMessages = this.props.messages.ownMessages;

		if (messages !== null) {
			messages.sort((first, second) => new Date(first.createdAt) - new Date(second.createdAt));
			ownMessages.sort((first, second) => new Date(first.createdAt) - new Date(second.createdAt));
		}

		return (
			<div className='message-list'>
				{isLoading ?
					<Preloader/> :
					<div>
						{messages.map(item => <Message
							key={Date.now() + item.id}
							message={item}
						/>)}
						<hr className='messages-divider'/>
						{ownMessages.map(item => <OwnMessage
								key={Date.now() + Math.random()}
								ownMessages={item}
								onDeleteMessage={(id) => this.props.onDeleteMessage(id)}
								onChangeOwnMessage={(id, text) => this.props.onChangeOwnMessage(id, text)}
							/>
						)}
						<input ref={input => input && input.focus()}/>
					</div>
				}

			</div>
		);
	}
}

export default MessageList;