import React, {Component} from 'react';
import './message.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faThumbsUp} from '@fortawesome/free-solid-svg-icons'
import Moment from "react-moment";

class Message extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			isLike: false,
		}
	}

	onLike() {
		this.setState({
			isLike: !this.state.isLike,
		})
	}

	render() {

		const {message} = this.props;
		const {isLike} = this.state;
		const likeStyle = isLike ? 'message-liked-icon' : 'like-icon';
		const likeStyleButton = this.state.isLike ? 'message-liked' : 'message-like';

		return (
			<div className='message'>
				<div className='message-user-avatar'>
					<img src={message.avatar} alt="avatar"/>
				</div>
				<div className='message-text'>
					{message.text}
				</div>

				<div className='user-data-block'>
					<div className='message-user-name'>{message.user}</div>
					<div className='message-time'>
						<Moment
							style={{color: '#775'}}
							format="HH:mm"
						>
							{message.createdAt}
						</Moment>
					</div>
				</div>

				<div className='message-like-block'>
					<button
						className={likeStyleButton}
						onClick={() => this.onLike()}
					>
						<FontAwesomeIcon
							icon={faThumbsUp}
							className={likeStyle}

						/>
					</button>
				</div>
			</div>
		);
	}
}

export default Message;